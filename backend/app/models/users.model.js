const mongoose = require('mongoose');

const UsersSchema = mongoose.Schema({
    first_name: String,
    last_name: String,
    role: String,
    email: String,
    status: String
}, {
    timestamps: true
});

module.exports = mongoose.model('Users', UsersSchema);